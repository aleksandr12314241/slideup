using UnityEngine;
using Zenject;
using Utility;

namespace Common {
    public class CoinsFlyController : MonoBehaviour {
        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        private RectTransform cointTempRT;

        [Inject]
        readonly CoinFly.Factory coinFlyFactory;
        [Inject]
        private CoinsUI coinsUI;
        [Inject]
        private ProfileLevel profileLevel;

        internal void AddCoins(int coins, Vector3 fieldPosition) {
            Vector2 viewportPosition = Camera.main.WorldToViewportPoint(fieldPosition);
            Vector2 WorldObject_ScreenPosition = new Vector2(
                ((viewportPosition.x * myRectTransform.rect.width) - (myRectTransform.rect.width * 0.5f)),
                ((viewportPosition.y * myRectTransform.rect.height) - (myRectTransform.rect.height * 0.5f))
            ) + new Vector2(-100f, -100f);

            cointTempRT.anchoredPosition = WorldObject_ScreenPosition;

            AddCoins(coins, cointTempRT);
        }

        internal void AddCoins(int crystals, RectTransform crystalRectTransform) {
            Vector2 startFly = CanvasPositionConvector.SwitchToRectTransform(crystalRectTransform, myRectTransform);
            Vector2 endFly = CanvasPositionConvector.SwitchToRectTransform(coinsUI.CoinImageRT, crystalRectTransform);

            int maxAddCrystals = 20;
            int addCrystals = crystals;

            int a1 = addCrystals / maxAddCrystals;
            int a2 = addCrystals % maxAddCrystals;

            int[] crystalsArray = new int[addCrystals > 20 ? 20 : addCrystals];

            for (int i = 0; i < crystalsArray.Length; i++) {
                int additional = 1;
                if (a2 <= 0) {
                    additional = 0;
                }
                crystalsArray[i] = a1 + additional;
                a2--;

                CoinFly c = coinFlyFactory.Create(startFly, endFly, crystalsArray[i]);
                c.OnEndFly = (int args) => { };
                c.OnEndFly += OnEndFly;
            }
        }

        void OnEndFly(int crystalsCount) {
            profileLevel.AddCoins(crystalsCount);
        }
    }
}