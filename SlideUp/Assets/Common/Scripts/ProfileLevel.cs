using System;

namespace Common {
    public class ProfileLevel {
        internal event Action<int, int> OnUpdateCoins = (int oldCoins, int newCoins) => { };

        private int coins = 0;

        internal int Coins {
            get => coins;
        }

        private ProfileLevel() {
            coins = 12;
        }

        internal void AddCoins(int additionalCoins) {
            int oldCoins = coins;

            coins += additionalCoins;

            OnUpdateCoins(oldCoins, coins);
        }
    }
}