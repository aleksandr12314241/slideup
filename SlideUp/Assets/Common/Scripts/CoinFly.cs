using UnityEngine;
using Zenject;
using DG.Tweening;
using System;

namespace Common {
    public class CoinFly : MonoBehaviour, IPoolable<Vector2, Vector2, int, IMemoryPool> {
        internal Action<int> OnEndFly = (int args) => { };

        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        RectTransform containerRectTransform;
        [SerializeField]
        private Vector2 mimMaxTimeAnim = new Vector2(0f, 0.3f);

        IMemoryPool _pool;

        Vector2 startPosition;
        Vector2 endPosition;
        int additionalCrystals;

        public void OnSpawned(Vector2 startPosition, Vector2 endPosition, int additionalCrystals, IMemoryPool pool) {
            this.startPosition = startPosition;
            this.additionalCrystals = additionalCrystals;

            myRectTransform.anchoredPosition = startPosition;

            _pool = pool;

            containerRectTransform.localScale = new Vector3(0f, 0f, 1f);

            //float timeAnim = 0.3f;
            //float appearanceTime = timeAnim + UnityEngine.Random.Range(0f, 0.3f);
            float timeAnim = mimMaxTimeAnim.y;
            float appearanceTime = timeAnim + UnityEngine.Random.Range(mimMaxTimeAnim.x, mimMaxTimeAnim.y);

            containerRectTransform.DOScale(new Vector3(0f, 0f, 1f), appearanceTime).OnComplete(() => {
                containerRectTransform.DOScale(new Vector3(0.5f, 0.5f, 1f), timeAnim);

                float positionOffset = 150f;

                Vector2 crystalPosition = startPosition + new Vector2(UnityEngine.Random.Range(-1 * positionOffset, positionOffset), UnityEngine.Random.Range(-1 * positionOffset, positionOffset));
                myRectTransform.DOAnchorPos(crystalPosition, timeAnim).OnComplete(() => {
                    Fly(endPosition);
                });
            });
        }

        public void OnDespawned() {
            _pool = null;
        }

        internal void Fly(Vector2 point) {
            float timeAnim = 0.3f;
            float delayTime = 0.1f + UnityEngine.Random.Range(0f, 0.6f);
            Vector2 myPosition = myRectTransform.anchoredPosition;

            myRectTransform.DOAnchorPos(myPosition, delayTime).OnComplete(() => {
                containerRectTransform.DOScale(new Vector3(1f, 1f, 1f), timeAnim * 2f);

                float jumpPowerOffset = point.y / 6f;
                float jumpPower = -point.y / 2f + UnityEngine.Random.Range(-jumpPowerOffset, jumpPowerOffset);

                jumpPower = UnityEngine.Random.Range(-500f, 500f);

                myRectTransform.DOAnchorPos(point, timeAnim * 2f).OnComplete(() => {
                    OnEndFly(additionalCrystals);

                    _pool.Despawn(this);
                });
            });
        }

        public class Factory : PlaceholderFactory<Vector2, Vector2, int, CoinFly> {
        }

        public class CoinPool : MonoPoolableMemoryPool<Vector2, Vector2, int, IMemoryPool, CoinFly> {
        }
    }
}