using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Common {
    public class CoinsUI : MonoBehaviour {
        [SerializeField]
        private Text coinsText;
        [SerializeField]
        private RectTransform coinImageRT;

        [Inject]
        private ProfileLevel profileLevel;

        internal RectTransform CoinImageRT {
            get => coinImageRT;
        }

        private void Start() {
            profileLevel.OnUpdateCoins += OnUpdateCoins;

            OnUpdateCoins(profileLevel.Coins, profileLevel.Coins);
        }

        private void OnDestroy() {
            profileLevel.OnUpdateCoins -= OnUpdateCoins;
        }

        private void OnUpdateCoins(int oldCoins, int newCoins) {
            coinsText.text = newCoins.ToString();
        }
    }
}