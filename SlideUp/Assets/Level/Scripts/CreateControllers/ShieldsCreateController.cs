using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Level {
    public class ShieldsCreateController : CreateController {
        [Inject]
        private readonly Enemy.ShieldFactory shieldFactory;

        private List<Shield> shields = new List<Shield>();

        protected override void Init() {
        }

        internal void Restart() {
            while (shields.Count > 0) {
                Shield e = shields[0];
                e.OnShieldDie -= OnShieldDie;
                shields.Remove(e);
                e.Dispose();
            }

            StopCreations();
            StartCreations();
        }

        protected override void CreateFallObjectPerform() {
            float enemyPositionX = Random.Range(fallObjectXPos.x, fallObjectXPos.y);

            Vector3 shieldPos = new Vector3(enemyPositionX, 0f, 0f);
            Shield shield = shieldFactory.Create(shieldPos);
            shield.OnShieldDie += OnShieldDie;

            shields.Add(shield);

            signalBus.Fire(new OnShieldCreate() {
                shieldPos = shieldPos,
                delay = delayFall
            });
        }

        private void OnShieldDie(Shield shield) {
            shield.OnShieldDie -= OnShieldDie;

            shields.Remove(shield);

            shield.Dispose();
        }
    }
}
