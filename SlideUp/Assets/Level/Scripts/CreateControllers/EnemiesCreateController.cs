using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Level {
    public class EnemiesCreateController : CreateController {

        [Inject]
        private SignalBus signalBus;
        [Inject]
        private readonly Enemy.EnemyFactory enemyFactory;

        private List<Enemy> enemies = new List<Enemy>();

        protected override void Init() {
        }

        internal void Restart() {
            while (enemies.Count > 0) {
                Enemy e = enemies[0];
                e.OnEnemyDie -= OnEnemyDie;
                e.OnKnockOutCoins -= OnKnockOutCoins;
                enemies.Remove(e);
                e.Dispose();
            }

            StopCreations();
            StartCreations();
        }

        protected override void CreateFallObjectPerform() {
            float enemyPositionX = Random.Range(fallObjectXPos.x, fallObjectXPos.y);

            Vector3 enemyPos = new Vector3(enemyPositionX, 0f, 0f);
            Enemy enemy = enemyFactory.Create(enemyPos);
            enemy.OnEnemyDie += OnEnemyDie;
            enemy.OnKnockOutCoins += OnKnockOutCoins;

            enemies.Add(enemy);

            signalBus.Fire(new OnEnemyCreate() {
                enemyPos = enemyPos,
                delay = delayFall
            });
        }

        private void OnEnemyDie(Enemy enemy) {
            enemy.OnEnemyDie -= OnEnemyDie;
            enemy.OnKnockOutCoins -= OnKnockOutCoins;

            enemies.Remove(enemy);

            enemy.Dispose();
        }

        private void OnKnockOutCoins(int minusCoins, Vector3 enemyPos) {
            signalBus.Fire(new OnAddCoins() {
                additionalCoins = -minusCoins
            });

            //signalBus.Fire(new OnAddCoinsLevel() {
            //    additionalCoins = -minusCoins,
            //    fieldPosition = enemyPos
            //});
        }
    }
}