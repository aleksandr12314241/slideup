using System.Collections;
using UnityEngine;
using Zenject;

namespace Level {

    public abstract class CreateController : MonoBehaviour {
        [SerializeField]
        protected float timeOfAppearance = 2f;
        [SerializeField]
        protected float delayFall = 1f;
        [SerializeField]
        protected Vector2 fallObjectXPos = new Vector2(-2.45f, 2.45f);

        [Inject]
        private readonly Enemy.EnemyFactory enemyFactory;
        [Inject]
        protected readonly SignalBus signalBus;

        private bool isActive = true;
        private Coroutine fallObjectCreations;

        private void Start() {
            StartCreations();

            Init();
        }

        protected abstract void Init();

        private void OnEnable() {
            isActive = true;

            StartCreations();
        }

        private void OnDisable() {
            isActive = false;

            StopCreations();
        }

        protected void StartCreations() {
            if (fallObjectCreations != null) {
                return;
            }

            fallObjectCreations = StartCoroutine(EnemyAppearanceUpdate());
        }

        protected void StopCreations() {
            if (fallObjectCreations != null) {
                StopCoroutine(fallObjectCreations);
                fallObjectCreations = null;
            }
        }

        private IEnumerator EnemyAppearanceUpdate() {
            while (isActive) {
                yield return new WaitForSeconds(timeOfAppearance);

                CreateFallObject();
            }
        }

        protected abstract void CreateFallObjectPerform();

        private void CreateFallObject() {
            CreateFallObjectPerform();
        }
    }
}