using Zenject;
using UnityEngine;
using Common;

namespace Level {
    public class LevelInstaller : MonoInstaller {
        [SerializeField]
        protected Transform enemiesTransform;
        [SerializeField]
        protected Transform shieldsTransform;

        [Inject]
        private LevelPrefabsDataSet levelPrefabsDataSet;

        public override void InstallBindings() {
            Container.Bind<ProfileLevel>().AsSingle().NonLazy();

            Container.Bind<SliderUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<SpeedInfoUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<CoinsUI>().FromComponentInHierarchy().AsSingle();

            Container.Bind<Player>().FromComponentInHierarchy().AsSingle();
            Container.Bind<LevelController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<EnemiesCreateController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<FallObjectDetectedUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<CoinsFlyController>().FromComponentInHierarchy().AsSingle();

            InstalCoinsPoolEnemies();
            InstalObjectsPoolEnemies();

            InitSignals();
        }

        private void InstalCoinsPoolEnemies() {
            Container.BindFactory<Vector2, Vector2, int, CoinFly, CoinFly.Factory>()
                .FromPoolableMemoryPool<Vector2, Vector2, int, CoinFly, CoinFly.CoinPool>(poolBinder => poolBinder
                .WithInitialSize(20)
                .FromComponentInNewPrefab(levelPrefabsDataSet.coinFlyPrf)
                .UnderTransform(FindObjectOfType<CoinsFlyController>().transform));
        }

        private void InstalObjectsPoolEnemies() {
            Container.BindFactory<Vector3, Enemy, Enemy.EnemyFactory>()
                .FromPoolableMemoryPool<Vector3, Enemy, Enemy.EnemyPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.enemyPrf)
                .UnderTransform(enemiesTransform));

            Container.BindFactory<Vector3, Shield, Enemy.ShieldFactory>()
                .FromPoolableMemoryPool<Vector3, Shield, Enemy.ShieldPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.shieldPrf)
                .UnderTransform(shieldsTransform));
        }

        private void InitSignals() {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<OnChangeSpeed>();
            Container.BindSignal<OnChangeSpeed>().ToMethod<Player>((x, s) => x.OnChangeSpeed(s.speedPercent)).FromResolve();
            Container.BindSignal<OnChangeSpeed>().ToMethod<SpeedInfoUI>((x, s) => x.OnChangeSpeed(s.speedPercent)).FromResolve();

            Container.DeclareSignal<OnEnemyCreate>();
            Container.BindSignal<OnEnemyCreate>().ToMethod<FallObjectDetectedUI>((x, s) => x.OnEnemyCreate(s.enemyPos, s.delay)).FromResolve();

            Container.DeclareSignal<OnShieldCreate>();
            Container.BindSignal<OnShieldCreate>().ToMethod<FallObjectDetectedUI>((x, s) => x.OnShieldCreate(s.shieldPos, s.delay)).FromResolve();

            Container.DeclareSignal<OnSetStartSpeed>();
            Container.BindSignal<OnSetStartSpeed>().ToMethod<SliderUI>((x, s) => x.OnSetStartSpeed(s.startSpeed, s.minMaxSpeed)).FromResolve();
            Container.BindSignal<OnSetStartSpeed>().ToMethod<SpeedInfoUI>((x, s) => x.OnSetStartSpeed(s.startSpeed, s.minMaxSpeed)).FromResolve();

            Container.DeclareSignal<OnAddCoins>();
            Container.BindSignal<OnAddCoins>().ToMethod<ProfileLevel>((x, s) => x.AddCoins(s.additionalCoins)).FromResolve();

            Container.DeclareSignal<OnAddCoinsLevel>();
            Container.BindSignal<OnAddCoinsLevel>().ToMethod<CoinsFlyController>((x, s) => x.AddCoins(s.additionalCoins, s.fieldPosition)).FromResolve();
        }
    }
}