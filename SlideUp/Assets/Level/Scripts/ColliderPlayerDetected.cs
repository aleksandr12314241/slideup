using UnityEngine;
using System;

namespace Level {
    [RequireComponent(typeof(Collider))]
    public class ColliderPlayerDetected : MonoBehaviour {
        internal event Action<Player> OnDetectPlayer = (Player player) => { };

        [SerializeField]
        private Collider myCollider;
        [SerializeField]
        private bool isAutoDamage;

        private const string playerTag = "Player";

        private void Start() {
            if (myCollider == null) {
                myCollider = GetComponent<Collider>();
            }
        }


#if UNITY_EDITOR
        private void OnValidate() {
            if (myCollider == null) {
                myCollider = GetComponent<Collider>();
            }
        }
#endif

        private void OnTriggerEnter(Collider other) {
            if (other.CompareTag(playerTag)) {
                Player player = other.GetComponent<Player>();

                OnDetectPlayer(player);
            }
        }

        private void OnTriggerExit(Collider other) {

        }

        internal void ActivateCollider() {
            myCollider.enabled = true;
        }
        internal void DiActivateCollider() {
            myCollider.enabled = false;
        }
    }
}