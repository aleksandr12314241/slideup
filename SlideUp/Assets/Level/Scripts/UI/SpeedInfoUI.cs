using UnityEngine;
using UnityEngine.UI;

namespace Level {
    [RequireComponent(typeof(Text))]
    public class SpeedInfoUI : MonoBehaviour {
        [SerializeField]
        private Text speedText;

        private Vector2 minMaxSpeed;

        internal void OnSetStartSpeed(float startSpeed, Vector2 minMaxSpeed) {
            this.minMaxSpeed = minMaxSpeed;

            speedText.text = startSpeed.ToString();
        }

        internal void OnChangeSpeed(float speedPercent) {
            float speed = minMaxSpeed.x + ((minMaxSpeed.y - minMaxSpeed.x) * ((speedPercent * 100f) / 100f));

            speedText.text = speed.ToString();
        }
    }
}