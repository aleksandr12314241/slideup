using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

namespace Level {
    public class FallObjectDetectedUI : MonoBehaviour {
        [SerializeField]
        private FallObjectDetectedItemData[] fallObjectDetectedItems;

        private RectTransform myRectTransform;

        private void Start() {
            myRectTransform = GetComponent<RectTransform>();

            Image[] fallObjectDetectedItemRTs = GetComponentsInChildren<Image>();

            fallObjectDetectedItems = new FallObjectDetectedItemData[fallObjectDetectedItemRTs.Length];

            for (int i = 0; i < fallObjectDetectedItems.Length; i++) {
                fallObjectDetectedItems[i].itemRT = fallObjectDetectedItemRTs[i].rectTransform;
                fallObjectDetectedItems[i].itemImage = fallObjectDetectedItemRTs[i];
                fallObjectDetectedItems[i].isActive = false;

                fallObjectDetectedItems[i].itemRT.gameObject.SetActive(false);
            }
        }

        internal void OnEnemyCreate(Vector3 enemyPos, float delay) {
            ShowFallObjectDetectedItem(enemyPos, delay, Color.red);
        }
        internal void OnShieldCreate(Vector3 shieldPos, float delay) {
            ShowFallObjectDetectedItem(shieldPos, delay, Color.yellow);
        }

        private void ShowFallObjectDetectedItem(Vector3 fallObjectPos, float delay, Color colorObject) {
            for (int i = 0; i < fallObjectDetectedItems.Length; i++) {
                if (fallObjectDetectedItems[i].isActive) {
                    continue;
                }

                ShowFallObjectDetectedItem(i, fallObjectPos, delay, colorObject);
                break;
            }
        }

        private void ShowFallObjectDetectedItem(int index, Vector3 enemyPos, float delay, Color colorObject) {
            Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(enemyPos);

            Vector2 WorldObject_ScreenPosition = new Vector2(
            ((ViewportPosition.x * myRectTransform.rect.width) - (myRectTransform.rect.width * 0.5f)), 0f);

            fallObjectDetectedItems[index].isActive = true;
            fallObjectDetectedItems[index].itemRT.gameObject.SetActive(true);
            fallObjectDetectedItems[index].itemRT.anchoredPosition = WorldObject_ScreenPosition;
            fallObjectDetectedItems[index].itemImage.color = colorObject;

            fallObjectDetectedItems[index].itemRT.DOAnchorPos(WorldObject_ScreenPosition, 1f).OnComplete(() => {
                fallObjectDetectedItems[index].itemRT.gameObject.SetActive(false);
                fallObjectDetectedItems[index].isActive = false;
            });
        }

        [Serializable]
        private struct FallObjectDetectedItemData {
            [SerializeField]
            internal RectTransform itemRT;
            [SerializeField]
            internal Image itemImage;
            [SerializeField]
            internal bool isActive;
        }
    }
}