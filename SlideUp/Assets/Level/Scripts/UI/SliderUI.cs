using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Level {
    public class SliderUI : MonoBehaviour {
        [SerializeField]
        private Slider slider;
        [SerializeField]
        private Text text;

        [Inject]
        private SignalBus signalBus;

        private void Start() {
            slider.onValueChanged.AddListener(ChangeSlider);
        }

        private void ChangeSlider(float value) {
            text.text = value.ToString();

            signalBus.Fire(new OnChangeSpeed() {
                speedPercent = value
            });
        }

        internal void OnSetStartSpeed(float startSpeed, Vector2 minMaxSpeed) {
            float speedPercentTemp = (startSpeed - minMaxSpeed.x) / (minMaxSpeed.y - minMaxSpeed.x);
            slider.value = speedPercentTemp;
        }
    }
}