using UnityEngine;

namespace Level {
    public class OnSetStartSpeed {
        internal float startSpeed;
        internal Vector2 minMaxSpeed;
    }
}