using UnityEngine;

namespace Level {
    public class OnEnemyCreate {
        internal Vector3 enemyPos;
        internal float delay;
    }
}