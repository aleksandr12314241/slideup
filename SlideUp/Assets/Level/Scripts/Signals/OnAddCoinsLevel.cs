using UnityEngine;

namespace Level {
    public class OnAddCoinsLevel {
        internal int additionalCoins;
        internal Vector3 fieldPosition;
    }
}
