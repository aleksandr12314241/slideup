using System;
using UnityEngine;
using Zenject;

namespace Level {
    public class LevelController : MonoBehaviour {
        [SerializeField]
        private Vector2[] path;
        [SerializeField]
        private LineRenderer lineRenderer;
        [SerializeField]
        private int coinsBonus = 1;

        [Inject]
        private SignalBus signalBus;
        [Inject]
        private Player player;
        [Inject]
        private EnemiesCreateController enemyController;

        private void Start() {
            Vector3[] pathTemp = new Vector3[path.Length];
            for (int i = 0; i < path.Length; i++) {
                pathTemp[i] = new Vector3(path[i].x, path[i].y, 0f);
            }

            lineRenderer.positionCount = path.Length;
            lineRenderer.SetPositions(pathTemp);

            player.Init(pathTemp);

            player.OnDie += OnDie;
            player.OnAddBonus += OnAddBonus;

            signalBus.Fire(new OnSetStartSpeed() {
                startSpeed = player.Speed,
                minMaxSpeed = player.MinMaxSpeed
            });
        }

        private void OnDestroy() {
            player.OnDie -= OnDie;
            player.OnAddBonus -= OnAddBonus;
        }

        private void OnAddBonus() {
            signalBus.Fire(new OnAddCoinsLevel() {
                additionalCoins = coinsBonus,
                fieldPosition = player.transform.position
            });
        }

        private void Restart() {
            player.Restart();
            enemyController.Restart();
        }

        private void OnDie() {
            Restart();
        }
    }
}