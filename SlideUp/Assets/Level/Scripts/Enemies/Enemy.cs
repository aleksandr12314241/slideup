using System;
using UnityEngine;
using Zenject;

namespace Level {
    public class Enemy : FallObject, IPoolable<Vector3, IMemoryPool> {
        internal event Action<Enemy> OnEnemyDie = (Enemy enemy) => { };
        internal event Action<int, Vector3> OnKnockOutCoins = (int coins, Vector3 enemyPos) => { };

        [SerializeField]
        private Vector2 minMaxReversMoveTime = Vector2.one;
        [SerializeField]
        private int minusCoins = 1;

        protected override void FallObjectDiePerform() {
            OnEnemyDie(this);
        }

        protected override void OnDetectPlayerPerform(Player player) {
            if (player.IsShield) {
                FallObjectDie();
                return;
            }

            float rversMoveTime = UnityEngine.Random.Range(minMaxReversMoveTime.x, minMaxReversMoveTime.y);
            player.ReversMove(rversMoveTime);

            OnKnockOutCoins(minusCoins, transform.position);

            FallObjectDie();
        }
    }
}