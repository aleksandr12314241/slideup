using UnityEngine;
using Zenject;
using System;

namespace Level {

    public class Shield : FallObject, IPoolable<Vector3, IMemoryPool> {
        internal event Action<Shield> OnShieldDie = (Shield shield) => { };

        [SerializeField]
        private Vector2 minMaxShieldTimeTime = Vector2.one;

        protected override void FallObjectDiePerform() {
        }

        protected override void OnDetectPlayerPerform(Player player) {
            float shieldTime = UnityEngine.Random.Range(minMaxShieldTimeTime.x, minMaxShieldTimeTime.y);

            player.AddShield(shieldTime);
        }
    }
}