using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Zenject;

namespace Level {

    public abstract class FallObject : MonoBehaviour {
        internal event Action<FallObject> OnFallObjectDie = (FallObject eallObject) => { };

        [SerializeField]
        private ColliderPlayerDetected colliderPlayerDetected;
        [SerializeField]
        private Rigidbody myRigidbody;

        protected IMemoryPool pool;

        private bool isAlive;

        public void OnSpawned(Vector3 enemyPos, IMemoryPool pool) {
            this.pool = pool;

            isAlive = true;

            transform.position = new Vector3(enemyPos.x, Camera.main.transform.position.y + 9f, 0f);

            myRigidbody.velocity = Vector3.zero;
            myRigidbody.angularVelocity = Vector3.zero;

            colliderPlayerDetected.OnDetectPlayer += OnDetectPlayer;
        }

        private void Update() {
            if (!isAlive) {
                return;
            }

            if (transform.position.y - Camera.main.transform.position.y < -9) {
                FallObjectDie();
            }
        }

        public void OnDespawned() {
            pool = null;

            colliderPlayerDetected.OnDetectPlayer -= OnDetectPlayer;
        }

        protected abstract void FallObjectDiePerform();
        protected void FallObjectDie() {
            isAlive = false;

            FallObjectDiePerform();
        }

        internal void Dispose() {
            colliderPlayerDetected.OnDetectPlayer -= OnDetectPlayer;
            pool.Despawn(this);
        }

        protected abstract void OnDetectPlayerPerform(Player player);

        private void OnDetectPlayer(Player player) {
            OnDetectPlayerPerform(player);
        }

        public class EnemyFactory : PlaceholderFactory<Vector3, Enemy> {
        }
        public class EnemyPool : MonoPoolableMemoryPool<Vector3, IMemoryPool, Enemy> {
        }

        public class ShieldFactory : PlaceholderFactory<Vector3, Shield> {
        }
        public class ShieldPool : MonoPoolableMemoryPool<Vector3, IMemoryPool, Shield> {
        }
    }
}