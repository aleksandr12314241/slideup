using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Level {

    public class CameraController : MonoBehaviour {
        // camera will follow this object
        [SerializeField]
        public Transform Target;

        // offset between camera and target
        [SerializeField]
        public Vector3 Offset;
        [SerializeField]
        private bool OffsetIsCameraPos;

        // change this value to get desired smoothness
        [SerializeField]
        public float SmoothTime = 0.3f;

        // This value will change at the runtime depending on target movement. Initialize with zero vector.
        private Vector3 velocity = Vector3.zero;

        private Coroutine currentFollowCameraUpdate;

        private Vector2 mouseDownPosition;
        private Vector2 mouseMostDeviation;
        private Vector2 offsetClick = new Vector2(50f, 50f);

        private float moveSpeed = 0.5f;

        private bool isDragCamera = false;
        internal bool IsDragCamera => isDragCamera;
        private bool isPlayerControlImmediately = false;

        private RectTransform canvasRectTransform;

        private void Start() {
            canvasRectTransform = FindObjectOfType<CanvasScaler>().GetComponent<RectTransform>();

            StartFollow();
        }

        internal void SetPlayer(Transform Target) {
            this.Target = Target;

            Offset = !OffsetIsCameraPos ? Offset : transform.position;
        }

        internal void StartFollow() {
            isDragCamera = false;

            StopFollow();

            if (Target == null) {
                return;
            }

            currentFollowCameraUpdate = StartCoroutine(FollowCameraUpdate());
        }

        private void StopFollow() {
            if (currentFollowCameraUpdate != null) {
                StopCoroutine(currentFollowCameraUpdate);
                currentFollowCameraUpdate = null;
            }
        }

        private IEnumerator FollowCameraUpdate() {
            while (!isDragCamera) {
                yield return null;

                //Target.position.x + 
                Vector3 targetPosition = new Vector3(Offset.x, Target.position.y + Offset.y, Offset.z);
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);
            }
        }
    }
}