using UnityEngine;
using Common;

namespace Level {

    [CreateAssetMenu(fileName = "LevelPrefabsDataSet", menuName = "SlideUp/LevelPrefabsDataSet")]
    public class LevelPrefabsDataSet : ScriptableObject {
        [SerializeField]
        internal Player playerPrf;
        [SerializeField]
        internal Enemy enemyPrf;
        [SerializeField]
        internal Shield shieldPrf;

        [SerializeField]
        internal CoinFly coinFlyPrf;
    }
}