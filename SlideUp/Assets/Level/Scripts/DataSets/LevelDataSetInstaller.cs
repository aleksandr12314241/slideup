using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Level {
    [CreateAssetMenu(fileName = "LevelDataSetInstaller", menuName = "SlideUp/LevelDataSetInstaller")]
    public class LevelDataSetInstaller : ScriptableObjectInstaller<LevelDataSetInstaller> {
        [SerializeField]
        private LevelPrefabsDataSet levelPrefabsDataSet;

        public override void InstallBindings() {
            Container.BindInstances(levelPrefabsDataSet);
        }
    }
}