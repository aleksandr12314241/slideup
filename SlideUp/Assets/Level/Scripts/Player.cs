using UnityEngine;
using System;
using System.Collections;

namespace Level {
    public class Player : MonoBehaviour {
        internal event Action OnHasReachedTarget = () => { };
        internal event Action OnDie = () => { };
        internal event Action OnAddBonus = () => { };

        [SerializeField]
        private GameObject shieldGO;

        [SerializeField]
        private Vector2 minMaxSpeed = new Vector2(2f, 10f);
        [SerializeField]
        private float speed = 2f;

        private Vector3[] path;
        private Vector3 targetPosition;
        private int indexPath;

        private bool isInit = false;
        private bool isShield = false;

        private int signMove = 1;
        private Coroutine reversMoveWait;
        private Coroutine shieldWait;

        internal bool IsShield {
            get => isShield;
        }
        internal Vector2 MinMaxSpeed {
            get => minMaxSpeed;
        }
        internal float Speed {
            get => speed;
        }

        internal void Init(Vector3[] path) {
            this.path = path;

            Restart();

            isInit = true;

            SetShield(false);
        }

        private void Update() {
            if (!isInit) {
                return;
            }

            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

            if (Vector3.Distance(transform.position, targetPosition) < step * 2) {
                OnHasReachedTarget();
                AddIndexPath();
            }
        }

        internal void AddIndexPath() {
            int oldIndexPath = indexPath;

            indexPath += signMove;
            if (indexPath >= path.Length) {

                indexPath = path.Length - 1;
                //����� ����
                return;
            }
            else if (indexPath < 0) {
                indexPath = 0;
                return;
            }

            if (indexPath > oldIndexPath && (minMaxSpeed.y - speed < 0.01f)) {
                AddBonus();
            }

            targetPosition = path[indexPath];
        }

        private void AddBonus() {
            OnAddBonus();
        }

        internal void Restart() {
            indexPath = 0;
            transform.position = path[0];
            AddIndexPath();
        }

        internal void SetTargetPosition(Vector3 targetPosition) {
            this.targetPosition = targetPosition;
        }

        internal void OnChangeSpeed(float speedPercent) {
            speed = minMaxSpeed.x + ((minMaxSpeed.y - minMaxSpeed.x) * ((speedPercent * 100f) / 100f));
        }

        internal void Die() {
            OnDie();
        }

        private void SetSignMove(int sign) {
            signMove = sign;

            AddIndexPath();
        }

        //reserv move
        public void ReversMove(float timeReversMove) {
            SetSignMove(-1);

            StartReversMove(timeReversMove);
        }

        private void StartReversMove(float timeReversMove) {
            StopReversMove();

            reversMoveWait = StartCoroutine(ReversMoveWait(timeReversMove));
        }
        private void StopReversMove() {
            if (reversMoveWait != null) {
                StopCoroutine(reversMoveWait);
                reversMoveWait = null;
            }
        }
        private IEnumerator ReversMoveWait(float timeReversMove) {
            yield return new WaitForSeconds(timeReversMove);

            SetSignMove(1);
        }

        //shield =========
        protected void SetShield(bool isShield) {
            this.isShield = isShield;

            shieldGO.SetActive(isShield);
        }

        public void AddShield(float timeShield) {
            StartShield(timeShield);
        }
        private void StartShield(float timeShield) {
            StopShield();

            SetShield(true);

            shieldWait = StartCoroutine(ShieldWait(timeShield));
        }
        private void StopShield() {
            if (shieldWait != null) {
                StopCoroutine(shieldWait);
                shieldWait = null;
            }
        }
        private IEnumerator ShieldWait(float timeShield) {
            yield return new WaitForSeconds(timeShield);

            SetShield(false);
        }
    }
}